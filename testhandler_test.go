package golanghowtotesthttprequests

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

// @test
func TestHandleGetTest_allowed_method(t *testing.T) {
	// use httptest server to mock connection
	// pass handler by http.HandlerFunc() adapter to satisfy http.Handler interface by adding missing method ServeHttp
	srv := httptest.NewServer(http.HandlerFunc(handleGetTest))

	// call GET on server
	resp, err := http.Get(srv.URL)
	if err != nil {
		t.Error(err)
	}
	defer resp.Body.Close()
	// expect status 200 ok
	if resp.StatusCode != http.StatusOK {
		t.Errorf("expected response status %v bu got %v instead", http.StatusOK, resp.StatusCode)
	}
	// expect response test

	expected := "test"
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Error(err)
	}
	if string(b) != expected {
		t.Errorf("expected response: %v but got: %v instead", expected, string(b))
	}
}
