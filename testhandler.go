package golanghowtotesthttprequests

import "net/http"

// Just some simple GET request handler
func handleGetTest(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("test"))
}
